autoscale:true
theme: Courier, 8

# Intro to GitLab
## DevOps on a Shell Script Budget
![filtered 150%](images/gitlab_no_words.png)

^ Goal: show how GitLab is an attactive option for DevOps workflows

^ Especially for smaller organizations with smaller budgets

---
# Mac Justice
## Slack, Tweets, etc.: @macjustice

^ Joke about name

---
![fit left](images/synapse_bar.png)

---
## What's GitLab?

^ I'm going to break this into two parts

^ An overview of GitLab, the product

---
## How I use GitLab

^ How I got into using GitLab, and what I do with it

<!-- ---
## Demo

^ If we have time and the spirits permit -->

---
# What's GitLab?
![filtered fit](images/gitlab_no_words.png)

^ GitLab is a web app, core feature is hosting git repos

^ GitHub competitor

^ Open Source, developed in the open on GitLab.com

^ Gitlab.com

^ Self-Hosted < Most popular

^ Freemium

^ Frequently updated

---
![fit](images/gitlab-screenshots/projects.png)

^ Here's some of the high level features

^ Just like with GitHub, you can store git repos there

^ Clone, push, etc.

^ Browse and edit in browser

^ Definition of "Project" in GitLab

---
![fit](images/gitlab-screenshots/merge-request.png)

^ Again like GitHub, you can create merge requests, so you and your team can review changes going into production.

---
![fit](images/gitlab-screenshots/merge-conflict.png)

^ If your merges have conflicts, you can resolve them right in the browser

---
![fit](images/gitlab-screenshots/issues.png)

^ Built in Issue Tracker, like Jira or FogBugz

^ You can attach issues to commits, CI builds, merge requests

^ Assign them to members of your team

---
![fit](images/gitlab-screenshots/issue-boards.png)

^ And there's even a neat Trello style Kanban board for visualizing your open issues.

---
![fit](images/gitlab-screenshots/ci.png)

^ One of my personal favorites is the CI pipeline tool

^ Like Jenkins, but different

^ more to come

---
![fit](images/gitlab-screenshots/container-registry.png)

^ And for kicks there's even a container registry

---
# How do I get it?

^ So let's talk really quick about what it takes to run your own GitLab server.

---
# Installation

- "Omnibus" package for Linux
- Docker Container
- Pre-built VMs (Amazon EC2/LightSail, Digital Ocean)

^ At Synapse we host GitLab on an Ubuntu VM in our VMware cluster, most common Linux flavors are supported

^ Docker container

^ Reccomend trying AWS or Digital Ocean if you want a running GitLab instance set up in just a minute or two.

^ Raspberry Pi

---
# Self-Hosted Pricing

Edition | Price | Support | Features
---|---|---|---
Community | Free | 'Community' | All major functionality
Enterprise Starter | $40/user/year | Next Day | Finer permissions
Enterprise Premium | $200/user/year | 4 Hour | High Availability, other advanced features

^ Community edition has all the features I'm going to describe today.

^ We upgraded to Starter after 5 years of use for support and some additional permissions control

<!-- ---
# Gitlab.com Pricing
Edition | Price/Year | Support | Features
---|---|---|---
Free | Free | 'Community' | 2,000 CI minutes, CE
Bronze | $48/user | Next Day | 2,000 CI minutes, EES
Silver | $228/user | 4 Hour | 10,000 CI minutes, EEP
Gold | $1188/user | 4 Hour | 50,000 CI minutes, EEP

^ I added this because of a conversation I had with Tom Bridge yesterday.

^ Free tier includes unlimited public and private repos -->

---
# Authentication & Authorization
- LDAP
- OAuth & SAML
- Kerberos

^ More than one

^ At Synapse, we use G Suite SAML for employee login, cross-referenced with LDAP for group permissions.

^ Synapse customers use Google Oauth via the OmniAuth feature.

---
# Integrations
## Chat

![inline](images/hipchat.png)![inline](images/slack.png)![inline](images/teams.png)

^ Prebuilt integrations for the major chat services

^ So you can get notifications for pipelines completed or issues assigned to you

---
# Integrations
## External CI

![inline fit](images/jenkins.png)![inline](images/bamboo.png)

^ You're not required to use GitLab's CI, if you already have one you like you can tie it in.

---
# Integrations
## Kubernetes
![inline](images/kubernetes.png)

^ I don't use Kubernetes myself, but GL has been spending a lot of resources making sure GL is closely integrated.

---
# How I use GitLab

^ That's the general overview, now I'm going to give you details on how I use it.

---
![inline 100%](images/git.png)

^ The firmware developers I work with need a version control system, and since many work with embedded Linux they're very comfortable with git.

^ So, they looked into setting up a central git repo host.

---
# 2009?
![80% right](images/gitolite.jpg)

^ First, Synapse used Gitolite, a free open source tool which gets the job done but with very few frills.

^ It has no GUI, so user interaction is strictly via ssh. It's pretty much just a git host with access control features.

---
# 2012
![right 85%](images/old_gitlab.png)

^ Around 2012, Synapse was getting bigger, we needed a better tool: collaboration, web UI, etc.

^ We picked GitLab largely because it was free and easy to set up.

^ Also because the guy doing the choosing liked Rails.

---
![inline 95%](images/munki.png)![inline](images/git.png)

^ In the following years, I had set up Munki and was interested in keeping my repo in git.

^ If you're not familiar with how Munki is structured, a repo is generally composed of a bunch of plist files and packages.

^ The plists are easy to track with git

^ But git doesn't like big files like packages.

---
# 2014: GitLab & Munki 1.0
![inline 110%](images/gitlab-munki-1.png)

^ Here's my first git-enabled munki setup

^ Problems

^ Very manual

^ Easy to accidentally conflict pkg changes with another user, if they didn't sync up with the git host AND server first

---
# INTERLUDE: Git LFS

^ Other people wanted to track big files with git, too

^ Git Fat, Git Annex

^ GitHub announced Git LFS, and shortly afterward GitLab announced they would support it.

---
#[fit]Git + ![inline](images/package.png)![inline](images/dmg.png) = :cold_sweat:

^ Because Git is designed for text files, it's not great at managing binary files, especially large ones.

---
#[fit]Git LFS + ![inline](images/package.png)![inline](images/dmg.png) = :heart_eyes:

^ Git LFS, however, does just fine

---
![fit](images/gitlfs.png)

^ Here's a rough sketch of how LFS works.

^ Enabling it in a GitLab project is as simple as clicking a checkbox and making sure you have sufficient space on your server. You can limit how much space each project has for LFS storage.

^ Git LFS can handle pretty big files. The biggest I've had cause to use was a 7.7GB El Capitan AutoDMG image, which was no trouble at all.

---
# Git LFS Example
## Local Installation
```bash
brew install git-lfs
git lfs install
```

^ Installing git lfs is easy. It's available on brew and MacPorts, or you can download the binary from GitHub.

^ the second command modifies your global git config to support LFS

---
# Git LFS Example
## Repo Setup
```bash
cd munki-repo
git lfs track "*.pkg"
git add .gitattributes
git commit -m "Added LFS tracking for PKGs"
```

^ Now that LFS is installed, you can just go to a repo and tell it which files to track. The pattern is added to .gitattributes, and once you commit that change you're in business.

---
# Git LFS Example
## Everyday Use
```bash
git add pkgs/SweetApp.pkg
git commit -m "Added SweetApp"
git push origin master
```

^ From here it's as easy as using standard git commands to commit, push and pull.

^ I tend to use MunkiAdmin for day to day work, so I make my changes there and commit them in a separate git app. Fork is my current favorite.

---
# 2016
![right fit](images/gitlab_no_words.png)

^ Back to the story

^ Our GitLab hadn't gotten any attention for a few years, so I got it upgraded to current, with the new logo, so I could get some of that sweet sweet LFS

^ First thing I did was rebuild my Munki workflow.

---
# 2016
![inline 95%](images/gitlab-munki-2.png)

^ Here was the new workflow. It was pretty good!

^ Cron on server

^ Everything was in LFS

^ Collaboration worked!

---
# INTERLUDE: CI

^ Now another interlude, I want to explain CI, and GitLab's approach.

---
# Continuous Integration

1. Push code to GitLab ⌨️ ⬆️
2. Do something with that code ⚙️  🛠
3. Report results 🚫 ✅

<!-- ## Continuous Deployment
- Push code to GitLab :arrow_right: Build, Test :arrow_right:  -->

^ In software development, CI means you push your code, and it gets tested.

^ Essentially though, CI tools are really a simple automation tool

^ When X happens, do Y

---
# Continuous Deployment/Delivery
1. Push code to GitLab ⌨️ ⬆️
2. Test that code ⚗ 🔬 🚫 ✅
3. If the tests pass, put it into production ⚙️ 🛠

^ The difference between CI and CD is more conceptual than anything.

^ The main difference between these is whether you want a human at the end giving the thumbs up.

---
# CI Runners

^ The tools that actually execute the CI tasks are called runners in GitLab.

---
# CI Runners

![inline](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Tux_Mono.svg/848px-Tux_Mono.svg.png)![inline 4.5%](images/macos.png)
![inline](images/Windows.png)

^ There are runners available for every major OS.

^ Once you have the runner installed, it will wait for GitLab to assign it jobs. Jobs can be run in sequence or parallel, and can be anything you can script.

^ Runners can be shared by many GitLab projects, or you can create runners reserved for specific projects, if a specific environment is needed.

^ You can tag runners, so if a certain job needs a certain environment, you tag the runner that is configured for it

---
# CI Runners

![inline](images/docker.png)

^ GitLab CI even supports Docker.

^ When you configure a runner to use docker, it will start a container of your choosing to excute a job in.

^ This is really handy, there's prebuilt containers with Python or AWS tools ready to use, and you can build your own and store them on GitLab for use.

---
# CI Runner Autoscaling

![inline 25%](images/aws.png)![inline](images/azure.png)
![inline](images/vmware.png)![inline 90%](images/digitalocean.png)


^ The GitLab runner also has an "autoscale" mode, which uses Docker Machine to create temporary VMs in a cloud provider or other hypervisor. The VMs execute their assigned jobs, return the results, and are terminated and deleted.

---
# Pipelines
![inline](images/pipelines.png)

^ Each task you include in CI is called a job.

^ More than one job makes a pipeline.

^ Pipelines execute in an order you define

^ Sequential / Parallel

^ If an earlier stage in the pipeline fails, the next stage doesn't start

---
# CI Output
![inline](images/ci_output.png)

^ How do you how things went?

^ All output from a CI job is visible in a console. Very helpful for troubleshooting.

---
# CI Configuration
## .gitlab-ci.yml
```yaml
validate:
  stage: test
  script: lint_roller.sh

roll_out:
  stage: deploy
  only: master
  script:
    - ./sound_klaxons_and_flash_lights.py
    - rsync build/* user@remote_server:/deploy/path/
```

^ My favorite part about GitLab CI is that you define your CI jobs as code, so changes to your build process are tracked in git too.

^ Here's an example of a CI config file. You just add one of these to your repo, and GitLab will try to start executing CI for your project on an available runner. You can call scripts and commands, specify stage order, set variables, pass files between stages, and lots more.

---
# .gitlab-ci.yml, annotated
```yaml
validate: # first job name
  stage: test # All 'test' stage jobs run before 'deploy' stage
  script: lint_roller.sh # Run this script

deploy: # second job name
  stage: deploy # Start only when all 'test' stage jobs complete
  only: master # Only run on Master branch
  script:
    - ./sound_klaxons_and_flash_lights.py # Call script in repo
    - rsync build/* user@remote_server:/deploy/path/ # inline command
```

^ In this example, I have two jobs, validate and roll-out. Validate is in the test stage, so it goes first, and it just runs the "check for typos" script. It will run any time someone pushes a commit to the parent GitLab project.

^ The second job, Roll-out, is marked as a deploy stage, so it only starts when all test jobs complete successfully. I specify to only run this job when there are updates to the master branch, because I don't want to push development branches to production. Finally, it runs a script, and an inline command.

^ If both jobs succeed, GitLab CI reports success.

---
# Secret Variables
![inline fit](images/secretvars.png)

^ One thing you don't want to do is put secret information, like private keys or API tokens in git.

^ CI Secret variables allow your CI jobs to access the info securely

^ Passed as Environment Vars to Runner

---
![](images/thenewway.jpg)

^ Now, you are ready to be taught the new way.

---
# 2017: TO THE CLOUD
![inline](images/gitlab-munki-3.png)

^ So, now I knew about CI.

^ Also, I figured out it was dumb to keep catalog files in git

^ And after going to MacDevOps Vancouver in 2016, I wanted to start serving my Munki repo from S3, because it was easy, cheap, and reliable.

---
![inline](images/imagr.png)

^ and from there I was off to the races

^ Imagr

^ updates to my imagr config, packages, and images are all tracked in git, distributed to my site imaging servers

---
![inline](images/profile.png)

^ Since the GitLab CI runner supports Mac OS, I set up a runner with an Apple developer certificate that signs all our configuration profiles. I also added a step to lint the plists and validate the contents of some fields, like PayloadOrganization. One time I slipped up and deployed a profile from "Your Org Here" to the whole company.

^ Next I want to figure out how to make the profiles get added to my Munki repo after being signed.

---
# Next Steps

---
# ? 🚗 ![23% inline](images/package.png) ?

---
# 🚗 ![23% inline](images/package.png)

^ AutoPkg is another good use case for CI. I haven't implemented this in GitLab CI myself yet, but Rick Heil has. You put all your overrides in a repo together, and use a nifty script from Facebook CPE to commit updated applications to your Munki repo.

---
![fit](images/ansible.png)

^ Synapse is trying to be an Ansible shop. I hope to accellerate this by automating Ansible Playbook runs through CI.

---
#🐮kins

---
# Mookins

^ Wes explained this at his talk yesterday, which is an integration between Munki, Oomnitza, his inventory tool, and Jenkins. Jenkins queries the hardware inventory to know who has which computer, and poof suddenly users have manifests that follow them from computer to computer. Watch his talk when it's posted, it was great.

<!-- ---
# DEMO TIME!!
![inline](images/demo.png) -->

<!-- ---
# [fit]Demo

^ Open .gitlab-ci.yml and s3-deploy.sh. Walk through the yml, the deploy script, and show how variables are set in the GitLab project outside of version control that can be passed into CI

^ Launch Mac Client VM. Prestaged with Managed Software Center, [s3-auth](https://github.com/waderobson/s3-auth) middleware script and settings, S3 bucket with R/W IAM user for CI and Read-Only user for Munki.

^ Open munki-repo in Munki Admin. Add package to site_default manifest. Save.

^ `cd gitlab_presentation`
`git add .`
`git commit -m "added pkg"`
`git push`

^ open https://gitlab.synapse.com/macj/gitlab_presentation
hit "pipelines" section
Open the log for the CI build

^ Once complete, check for updates in the Managed Software Center in client VM.
GREAT SUCCESS -->

---
# [fit] So

^ So why do all this?

---
# [fit] Automatic

^ Using GitLab and CI makes it much easier to do better IT.

^ Automation is good, but the more you can control the execution environment, the more reliable it is. With GitLab CI, it's really easy. You set up your runner environment, and then just send your tasks to it.

---
# [fit] Accountable

^ You get to see exactly what changed, who changed it, and when. You get your git history, as well as the execution transcripts of every job sent to CI.

---
# [fit] Collaborative

^ "Hey, are you on the server?" Nobody likes that question. By routing our workflows through GitLab, anybody can jump in the process. If our work conflicts, we find out before it gets into production.

---

# References
- PSU MacAdmins 2017
  - Tom Bridge - Munki Mistakes Made Right
  - Lucas Hall - Managing MacOS without MacOS (almost)
  - Wesley Whetstone - Continuous Integration: An automation framework for Mac Admins.
  - Rick Heil - Advanced Munki Infrastructure: Moving to Cloud Services
- MacDevOps:YVR 2016
  - [Tim Sutton on Jenkins CI](http://macdevops.ca/MDO2016/jenkins/Default.html)
  - [Wade Robson on Munki & S3](http://matx.ca/mdoyvr/2016/day2/munkimiddleware/Default.html)

---
# Thanks!
### Twitter: @macjustice
### MacAdmins Slack: macjustice
### Wherever: macjustice

---
# Q & A
